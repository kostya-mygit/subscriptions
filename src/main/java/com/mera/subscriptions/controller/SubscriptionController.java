package com.mera.subscriptions.controller;

import com.mera.subscriptions.model.Subscription;
import com.mera.subscriptions.model.SubscriptionRequest;
import com.mera.subscriptions.model.SubscriptionResponse;
import com.mera.subscriptions.service.SubscriptionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/subscriptions")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    @GetMapping
    ResponseEntity<List<SubscriptionResponse>> getAll() {
        log.info("--> Received GET ALL request");
        List<Subscription> subscriptions = subscriptionService.getAll();
        List<SubscriptionResponse> subscriptionResponseList = subscriptions.stream()
                .map(this::convertToSubscriptionResponse)
                .collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(subscriptionResponseList);
    }

    @GetMapping("/{id}")
    ResponseEntity<SubscriptionResponse> getById(@PathVariable String id) {
        log.info("--> Received GET request with id {}", id);
        SubscriptionResponse subscriptionResponse = convertToSubscriptionResponse(subscriptionService.getById(id));
        return ResponseEntity.status(HttpStatus.OK).body(subscriptionResponse);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        log.info("--> Received DELETE request with id {}", id);
        subscriptionService.delete(id);
    }

    @PostMapping
    ResponseEntity<SubscriptionResponse> create(@RequestBody SubscriptionRequest subscriptionRequest) {
        log.info("--> Received POST request with {}", subscriptionRequest);
        SubscriptionResponse subscriptionResponse = convertToSubscriptionResponse(subscriptionService.create(subscriptionRequest));
        return ResponseEntity.status(HttpStatus.CREATED).body(subscriptionResponse);
    }

    @PutMapping("/{id}")
    ResponseEntity<SubscriptionResponse> update(@PathVariable String id, @RequestBody SubscriptionRequest subscriptionRequest) {
        log.info("--> Received PUT request with id {} and {}", id, subscriptionRequest);
        SubscriptionResponse subscriptionResponse = convertToSubscriptionResponse(subscriptionService.update(id, subscriptionRequest));
        return ResponseEntity.status(HttpStatus.OK).body(subscriptionResponse);
    }

    private SubscriptionResponse convertToSubscriptionResponse(Subscription subscription) {
        return SubscriptionResponse.builder()
                .id(subscription.getId())
                .created(subscription.getCreated())
                .expires(subscription.getExpires())
                .isActive(subscription.isActive())
                .family(subscription.getFamily())
                .webhook(subscription.getWebhook())
                .build();
    }
}
