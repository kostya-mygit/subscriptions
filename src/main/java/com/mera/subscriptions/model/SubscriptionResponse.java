package com.mera.subscriptions.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionResponse {
    private String id;
    private LocalDateTime created;
    private LocalDateTime expires;
    private boolean isActive;
    private Family family;
    private String webhook;
}
