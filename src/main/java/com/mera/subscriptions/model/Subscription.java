package com.mera.subscriptions.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("subscription")
public class Subscription {
    @Id
    private String id;
    private LocalDateTime created;
    private LocalDateTime expires;
    private boolean isActive;
    private Family family;
    private String webhook;
}
