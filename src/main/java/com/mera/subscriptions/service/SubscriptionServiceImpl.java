package com.mera.subscriptions.service;

import com.mera.subscriptions.exception.SubscriptionNotFoundException;
import com.mera.subscriptions.model.*;
import com.mera.subscriptions.repository.SubscriptionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Slf4j
@RequiredArgsConstructor
@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionRepository repository;
    private final ProducerService producerService;

    @Override
    public List<Subscription> getAll() {
        return repository.findAll();
    }

    @Override
    public Subscription getById(String id) {
        Optional<Subscription> subscriptionOptional = repository.findById(id);
        if (subscriptionOptional.isEmpty()) {
            throw new SubscriptionNotFoundException(id);
        } else {
            Subscription subscription = subscriptionOptional.get();
            log.info("--> Found {}", subscription);

            produceMessage(Action.READ, subscription);

            return subscription;
        }
    }

    @Override
    public void delete(String id) {
        Optional<Subscription> subscriptionOptional = repository.findById(id);
        if (subscriptionOptional.isEmpty()) {
            throw new SubscriptionNotFoundException(id);
        } else {
            repository.deleteById(id);
            log.info("--> Deleted subscription with id {}", id);

            produceMessage(Action.DELETE, subscriptionOptional.get());
        }
    }

    @Override
    public Subscription create(SubscriptionRequest subscriptionRequest) {
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        Subscription subscription = Subscription.builder()
                .created(now)
                .expires(now.plusMinutes(new Random().nextInt(10) + 1))
                .isActive(true)
                .family(subscriptionRequest.getFamily())
                .webhook(subscriptionRequest.getWebhook())
                .build();
        Subscription savedSubscription = repository.save(subscription);
        log.info("--> Created {}", savedSubscription);

        produceMessage(Action.CREATE, savedSubscription);

        return savedSubscription;
    }

    @Override
    public Subscription update(String id, SubscriptionRequest subscriptionRequest) {
        Optional<Subscription> subscriptionOptional = repository.findById(id);
        if (subscriptionOptional.isEmpty()) {
            throw new SubscriptionNotFoundException(id);
        } else {
            Subscription subscription = subscriptionOptional.get();
            subscription.setFamily(subscriptionRequest.getFamily());
            subscription.setWebhook(subscriptionRequest.getWebhook());
            Subscription updatedSubscription = repository.save(subscription);
            log.info("--> Updated {}", updatedSubscription);

            produceMessage(Action.UPDATE, updatedSubscription);

            return updatedSubscription;
        }
    }

    @Override
    public void updateActivity(Subscription subscription, boolean isActive) {
        subscription.setActive(isActive);
        Subscription updatedSubscription = repository.save(subscription);
        log.info("--> Updated activity of {}", updatedSubscription);

        produceMessage(Action.UPDATE, updatedSubscription);
    }

    private void produceMessage(Action action, Subscription subscription) {
        producerService.produce(new SubscriptionRecord(
                action,
                subscription.getId(),
                subscription.getCreated(),
                subscription.getExpires(),
                subscription.isActive(),
                subscription.getFamily(),
                subscription.getWebhook()));
    }
}
