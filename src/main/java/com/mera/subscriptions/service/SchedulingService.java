package com.mera.subscriptions.service;

import com.mera.subscriptions.model.Subscription;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class SchedulingService {

    private final SubscriptionService subscriptionService;

    @Scheduled(fixedDelay = 60000)
    public void checkExpiredSubscriptions() {
        log.info("------> Checking expired subscriptions");
        List<Subscription> subscriptions = subscriptionService.getAll();
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        subscriptions.forEach(s -> {
            if (s.getExpires().isBefore(now) && s.isActive()) {
                subscriptionService.updateActivity(s, false);
            }
        });
    }
}
