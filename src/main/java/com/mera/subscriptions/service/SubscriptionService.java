package com.mera.subscriptions.service;

import com.mera.subscriptions.model.Subscription;
import com.mera.subscriptions.model.SubscriptionRequest;

import java.util.List;

public interface SubscriptionService {
    List<Subscription> getAll();
    Subscription getById(String id);
    void delete(String id);
    Subscription create(SubscriptionRequest subscriptionRequest);
    Subscription update(String id, SubscriptionRequest subscriptionRequest);
    void updateActivity(Subscription subscription, boolean isActive);
}
