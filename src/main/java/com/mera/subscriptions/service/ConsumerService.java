package com.mera.subscriptions.service;

import com.mera.subscriptions.model.SubscriptionRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ConsumerService {

    @KafkaListener(topics = "${topic.name}")
    public void consume(ConsumerRecord<String, SubscriptionRecord> record) {
        log.info("----> Received message {}", record.value());
    }
}
