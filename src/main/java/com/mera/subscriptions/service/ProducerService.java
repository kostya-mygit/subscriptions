package com.mera.subscriptions.service;

import com.mera.subscriptions.model.SubscriptionRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProducerService {

    @Value("${topic.name}")
    private String topicName;
    private final KafkaTemplate<String, SubscriptionRecord> kafkaTemplate;

    @Autowired
    public ProducerService(KafkaTemplate<String, SubscriptionRecord> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void produce(SubscriptionRecord message) {
        kafkaTemplate.send(topicName, message);
        log.info("----> Sent message {}", message);
    }
}
