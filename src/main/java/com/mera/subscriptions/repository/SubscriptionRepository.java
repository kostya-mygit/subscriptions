package com.mera.subscriptions.repository;

import com.mera.subscriptions.model.Subscription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionRepository extends CrudRepository<Subscription, String> {
    List<Subscription> findAll();
}
