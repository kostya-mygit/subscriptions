package com.mera.subscriptions;

import com.mera.subscriptions.model.Family;
import com.mera.subscriptions.model.Subscription;
import com.mera.subscriptions.model.SubscriptionRequest;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;

public class TestData {

    public static final String SUBSCRIPTION_1_ID = "e7888717-4ba4-477e-9727-297e051d217b";
    public static final String SUBSCRIPTION_2_ID = "fe250b18-8a4c-4f0e-9f9c-37181fa106a5";

    public static final Subscription SUBSCRIPTION_1 =
            new Subscription(SUBSCRIPTION_1_ID,
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 0),
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 5),
                    true, Family.ADMIN, "https://server.com/webhook");

    public static final Subscription SUBSCRIPTION_1_TO_UPDATE =
            new Subscription(SUBSCRIPTION_1_ID,
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 0),
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 5),
                    true, Family.ADMIN, "https://server.com/webhook");

    public static final Subscription SUBSCRIPTION_1_UPDATED =
            new Subscription(SUBSCRIPTION_1_ID,
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 0),
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 5),
                    true, Family.RECORDING, "https://test.com/webhook");

    public static final Subscription SUBSCRIPTION_1_ACTIVE =
            new Subscription(SUBSCRIPTION_1_ID,
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 0),
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 5),
                    true, Family.ADMIN, "https://server.com/webhook");

    public static final Subscription SUBSCRIPTION_1_NOT_ACTIVE =
            new Subscription(SUBSCRIPTION_1_ID,
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 0),
                    LocalDateTime.of(2021, Month.NOVEMBER, 11, 15, 5),
                    false, Family.ADMIN, "https://server.com/webhook");

    public static final Subscription SUBSCRIPTION_2 =
            new Subscription(SUBSCRIPTION_2_ID,
                    LocalDateTime.now(ZoneOffset.UTC),
                    LocalDateTime.now(ZoneOffset.UTC).plusMinutes(10),
                    true, Family.RECORDING, "https://test.com/webhook");

    public static final SubscriptionRequest SUBSCRIPTION_REQUEST_1 =
            new SubscriptionRequest(Family.ADMIN, "https://server.com/webhook");

    public static final SubscriptionRequest SUBSCRIPTION_REQUEST_2 =
            new SubscriptionRequest(Family.RECORDING, "https://test.com/webhook");

}
