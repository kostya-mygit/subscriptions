package com.mera.subscriptions.service;

import com.mera.subscriptions.exception.SubscriptionNotFoundException;
import com.mera.subscriptions.model.Action;
import com.mera.subscriptions.model.Subscription;
import com.mera.subscriptions.model.SubscriptionRecord;
import com.mera.subscriptions.repository.SubscriptionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.mera.subscriptions.TestData.*;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class SubscriptionServiceTest {

    @Mock
    private SubscriptionRepository subscriptionRepository;
    @Mock
    private ProducerService producerService;

    private SubscriptionService subscriptionService;

    @BeforeEach
    void setUp() {
        subscriptionService = new SubscriptionServiceImpl(subscriptionRepository, producerService);
    }

    @Test
    void getAll() {
        // given
        given(subscriptionRepository.findAll()).willReturn(List.of(SUBSCRIPTION_1, SUBSCRIPTION_2));

        // when
        List<Subscription> subscriptions = subscriptionService.getAll();

        // then
        then(subscriptions).hasSize(2);
        then(subscriptions).containsExactly(SUBSCRIPTION_1, SUBSCRIPTION_2);
        then(subscriptions).isEqualTo(List.of(SUBSCRIPTION_1, SUBSCRIPTION_2));
        verify(subscriptionRepository).findAll();
    }

    @Test
    void getById() {
        // given
        given(subscriptionRepository.findById(SUBSCRIPTION_1_ID)).willReturn(Optional.of(SUBSCRIPTION_1));

        // when
        Subscription subscription = subscriptionService.getById(SUBSCRIPTION_1_ID);

        // then
        then(subscription).isEqualTo(SUBSCRIPTION_1);
        verify(subscriptionRepository).findById(SUBSCRIPTION_1_ID);
        verify(producerService).produce(new SubscriptionRecord(
                Action.READ,
                subscription.getId(),
                subscription.getCreated(),
                subscription.getExpires(),
                subscription.isActive(),
                subscription.getFamily(),
                subscription.getWebhook()));
    }

    @Test
    void getByIdWithException() {
        // given
        given(subscriptionRepository.findById(SUBSCRIPTION_1_ID)).willReturn(Optional.empty());

        // when
        Throwable throwable = catchThrowable(() -> subscriptionService.getById(SUBSCRIPTION_1_ID));

        // then
        then(throwable).isInstanceOf(SubscriptionNotFoundException.class)
                .hasMessageContaining(SUBSCRIPTION_1_ID);
        verify(subscriptionRepository).findById(SUBSCRIPTION_1_ID);
        verify(producerService, never()).produce(any());
    }

    @Test
    void delete() {
        // given
        given(subscriptionRepository.findById(SUBSCRIPTION_1_ID)).willReturn(Optional.of(SUBSCRIPTION_1));

        // when
        subscriptionService.delete(SUBSCRIPTION_1_ID);

        // then
        verify(subscriptionRepository).findById(SUBSCRIPTION_1_ID);
        verify(subscriptionRepository).deleteById(SUBSCRIPTION_1_ID);
        verify(producerService).produce(new SubscriptionRecord(
                Action.DELETE,
                SUBSCRIPTION_1.getId(),
                SUBSCRIPTION_1.getCreated(),
                SUBSCRIPTION_1.getExpires(),
                SUBSCRIPTION_1.isActive(),
                SUBSCRIPTION_1.getFamily(),
                SUBSCRIPTION_1.getWebhook()));
    }

    @Test
    void deleteWithException() {
        // given
        given(subscriptionRepository.findById(SUBSCRIPTION_1_ID)).willReturn(Optional.empty());

        // when
        Throwable throwable = catchThrowable(() -> subscriptionService.delete(SUBSCRIPTION_1_ID));

        // then
        then(throwable).isInstanceOf(SubscriptionNotFoundException.class)
                .hasMessageContaining(SUBSCRIPTION_1_ID);
        verify(subscriptionRepository).findById(SUBSCRIPTION_1_ID);
        verify(producerService, never()).produce(any());
    }

    @Test
    void create() {
        // given
        given(subscriptionRepository.save(any(Subscription.class))).willReturn(SUBSCRIPTION_2);

        // when
        Subscription subscription = subscriptionService.create(SUBSCRIPTION_REQUEST_2);

        // then
        then(subscription).isEqualTo(SUBSCRIPTION_2);
        verify(subscriptionRepository).save(any(Subscription.class));
        verify(producerService).produce(new SubscriptionRecord(
                Action.CREATE,
                subscription.getId(),
                subscription.getCreated(),
                subscription.getExpires(),
                subscription.isActive(),
                subscription.getFamily(),
                subscription.getWebhook()));
    }

    @Test
    void update() {
        // given
        given(subscriptionRepository.findById(SUBSCRIPTION_1_ID)).willReturn(Optional.of(SUBSCRIPTION_1_TO_UPDATE));
        given(subscriptionRepository.save(SUBSCRIPTION_1_TO_UPDATE)).willReturn(SUBSCRIPTION_1_UPDATED);

        // when
        Subscription subscription = subscriptionService.update(SUBSCRIPTION_1_ID, SUBSCRIPTION_REQUEST_2);

        // then
        then(subscription).isEqualTo(SUBSCRIPTION_1_UPDATED);
        verify(subscriptionRepository).findById(SUBSCRIPTION_1_ID);
        verify(subscriptionRepository).save(SUBSCRIPTION_1_UPDATED);
        verify(producerService).produce(new SubscriptionRecord(
                Action.UPDATE,
                subscription.getId(),
                subscription.getCreated(),
                subscription.getExpires(),
                subscription.isActive(),
                subscription.getFamily(),
                subscription.getWebhook()));
    }

    @Test
    void updateWithException() {
        // given
        given(subscriptionRepository.findById(SUBSCRIPTION_1_ID)).willReturn(Optional.empty());

        // when
        Throwable throwable = catchThrowable(() -> subscriptionService.update(SUBSCRIPTION_1_ID, SUBSCRIPTION_REQUEST_2));

        // then
        then(throwable).isInstanceOf(SubscriptionNotFoundException.class)
                .hasMessageContaining(SUBSCRIPTION_1_ID);
        verify(subscriptionRepository).findById(SUBSCRIPTION_1_ID);
        verify(subscriptionRepository, never()).save(any());
        verify(producerService, never()).produce(any());
    }

    @Test
    void updateActivity() {
        // given
        given(subscriptionRepository.save(SUBSCRIPTION_1_NOT_ACTIVE)).willReturn(SUBSCRIPTION_1_NOT_ACTIVE);

        // when
        subscriptionService.updateActivity(SUBSCRIPTION_1_ACTIVE, false);

        // then
        verify(subscriptionRepository).save(SUBSCRIPTION_1_NOT_ACTIVE);
        verify(producerService).produce(new SubscriptionRecord(
                Action.UPDATE,
                SUBSCRIPTION_1_NOT_ACTIVE.getId(),
                SUBSCRIPTION_1_NOT_ACTIVE.getCreated(),
                SUBSCRIPTION_1_NOT_ACTIVE.getExpires(),
                SUBSCRIPTION_1_NOT_ACTIVE.isActive(),
                SUBSCRIPTION_1_NOT_ACTIVE.getFamily(),
                SUBSCRIPTION_1_NOT_ACTIVE.getWebhook()));
    }
}