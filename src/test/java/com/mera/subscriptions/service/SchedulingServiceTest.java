package com.mera.subscriptions.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.mera.subscriptions.TestData.SUBSCRIPTION_1;
import static com.mera.subscriptions.TestData.SUBSCRIPTION_2;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SchedulingServiceTest {

    @Mock
    private SubscriptionService subscriptionService;

    private SchedulingService schedulingService;

    @BeforeEach
    void setUp() {
        schedulingService = new SchedulingService(subscriptionService);
    }

    @Test
    void checkExpiredSubscriptions() {
        // given
        given(subscriptionService.getAll()).willReturn(List.of(SUBSCRIPTION_1, SUBSCRIPTION_2));

        // when
        schedulingService.checkExpiredSubscriptions();

        // then
        verify(subscriptionService).updateActivity(SUBSCRIPTION_1, false);
        verifyNoMoreInteractions(subscriptionService);
        verify(subscriptionService, times(1)).updateActivity(SUBSCRIPTION_1, false);
        verify(subscriptionService, times(0)).updateActivity(SUBSCRIPTION_2, false);
    }
}