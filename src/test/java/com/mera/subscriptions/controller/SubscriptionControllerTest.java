package com.mera.subscriptions.controller;

import com.mera.subscriptions.exception.SubscriptionNotFoundException;
import com.mera.subscriptions.model.Subscription;
import com.mera.subscriptions.model.SubscriptionRequest;
import com.mera.subscriptions.service.SubscriptionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static com.mera.subscriptions.TestData.*;
import static org.mockito.BDDMockito.given;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.willThrow;

@AutoConfigureJsonTesters
@WebMvcTest(SubscriptionController.class)
class SubscriptionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SubscriptionService subscriptionService;

    @Autowired
    private JacksonTester<Subscription> jsonSubscription;

    @Autowired
    private JacksonTester<List<Subscription>> jsonSubscriptionList;

    @Autowired
    private JacksonTester<SubscriptionRequest> jsonSubscriptionRequest;

    @Test
    void getAll() throws Exception {
        // given
        given(subscriptionService.getAll()).willReturn(List.of(SUBSCRIPTION_1, SUBSCRIPTION_2));

        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.get("/subscriptions")
                .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonSubscriptionList.write(List.of(SUBSCRIPTION_1, SUBSCRIPTION_2)).getJson());
    }

    @Test
    void getById() throws Exception {
        // given
        given(subscriptionService.getById(SUBSCRIPTION_1_ID)).willReturn(SUBSCRIPTION_1);

        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.get("/subscriptions/" + SUBSCRIPTION_1_ID)
                .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonSubscription.write(SUBSCRIPTION_1).getJson());
    }

    @Test
    void getByIdNotFound() throws Exception {
        // given
        given(subscriptionService.getById(SUBSCRIPTION_1_ID)).willThrow(new SubscriptionNotFoundException(SUBSCRIPTION_1_ID));

        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.get("/subscriptions/" + SUBSCRIPTION_1_ID)
                .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEmpty();
    }

    @Test
    void delete() throws Exception {
        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.delete("/subscriptions/" + SUBSCRIPTION_1_ID)
                .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.NO_CONTENT.value());
        then(response.getContentAsString()).isEmpty();
    }

    @Test
    void deleteNotFound() throws Exception {
        // given
        willThrow(new SubscriptionNotFoundException(SUBSCRIPTION_1_ID)).given(subscriptionService).delete(SUBSCRIPTION_1_ID);

        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.delete("/subscriptions/" + SUBSCRIPTION_1_ID)
                .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEmpty();
    }

    @Test
    void create() throws Exception {
        // given
        given(subscriptionService.create(SUBSCRIPTION_REQUEST_1)).willReturn(SUBSCRIPTION_1);

        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.post("/subscriptions")
                        .contentType(MediaType.APPLICATION_JSON).content(jsonSubscriptionRequest.write(SUBSCRIPTION_REQUEST_1).getJson()))
                .andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
        then(response.getContentAsString()).isEqualTo(jsonSubscription.write(SUBSCRIPTION_1).getJson());
    }

    @Test
    void update() throws Exception {
        // given
        given(subscriptionService.update(SUBSCRIPTION_1_ID, SUBSCRIPTION_REQUEST_1)).willReturn(SUBSCRIPTION_1);

        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.put("/subscriptions/" + SUBSCRIPTION_1_ID)
                        .contentType(MediaType.APPLICATION_JSON).content(jsonSubscriptionRequest.write(SUBSCRIPTION_REQUEST_1).getJson()))
                .andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(jsonSubscription.write(SUBSCRIPTION_1).getJson());
    }

    @Test
    void updateNotFound() throws Exception {
        // given
        given(subscriptionService.update(SUBSCRIPTION_1_ID, SUBSCRIPTION_REQUEST_1)).willThrow(new SubscriptionNotFoundException(SUBSCRIPTION_1_ID));

        // when
        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.put("/subscriptions/" + SUBSCRIPTION_1_ID)
                        .contentType(MediaType.APPLICATION_JSON).content(jsonSubscriptionRequest.write(SUBSCRIPTION_REQUEST_1).getJson()))
                .andReturn().getResponse();

        // then
        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).isEmpty();
    }
}