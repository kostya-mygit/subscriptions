FROM openjdk:11
COPY ./target/subscriptions-0.0.1-SNAPSHOT.jar /usr/src/subscriptions/
WORKDIR /usr/src/subscriptions
EXPOSE 8082
CMD ["java", "-jar", "subscriptions-0.0.1-SNAPSHOT.jar"]